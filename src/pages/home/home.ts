import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import {Platform} from 'ionic-angular';
//import { DatabaseProvider } from '../../providers/database/database';

import { SqliteProvider } from '../../providers/sqlite/sqlite';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'//, providers:[DatabaseProvider]
})
export class HomePage {
  user: any;
  pass: any;
   public todos = [];
  public text : any;
  constructor(public navCtrl: NavController,
    public alertCtrl: AlertController,
    public sqliteService: SqliteProvider,
     protected platform : Platform) {
    this
      .platform
      .ready()
      .then(() => {
       

 let alert = this.alertCtrl.create({
      title: 'no created!',
      subTitle:  this.sqliteService.openDb()+" ok",
      buttons: ['OK']
    });
    alert.present();

        this
          .sqliteService
          .getRows()
          .then(s => {
            this.todos = this.sqliteService.arr;
          }).catch(function(){

            
          });
      });
  }
  //Adding the Function
  add(i) {
    this
      .sqliteService
      .addItem(i)
      .then(s => {
        this.todos = this.sqliteService.arr;
        this.text = '';
      }).catch(a=>{
         let alert = this.alertCtrl.create({
      title: 'no inserted',
      subTitle:"ok",
      buttons: ['OK']
    });
    alert.present();
      });
  }
  //Deleting function
  delete(i) {
    this
      .sqliteService
      .del(i)
      .then(s => {
        this.todos = this.sqliteService.arr;
      });
  }
  //updating function
  update(id, todo) {
    var prompt = window.prompt('Update', todo);
    this
      .sqliteService
      .update(id, prompt)
      .then(s => {
        this.todos = this.sqliteService.arr;
      });
  }


  userCheck() {

    let alert = this.alertCtrl.create({
      title: 'New Friend!',
      subTitle: this.user + " " + this.pass,
      buttons: ['OK']
    });

    //ionic alert.present();
    // this.sql.make();


  }
  
  ngOnInit() {

  }



  /*
    sqlCreate() {
      let alert = this.alertCtrl.create({
        title: ' !',
        subTitle: "nothing to do here",
        buttons: ['OK']
      });
  this.sqlite.create({
    name: 'data.db',
    location: 'default'
  }).then(function(){
    
  alert.setTitle("ok"+this.sqlite);
      alert.present();
  }).catch(function(){
  alert.setTitle("NO ok");
    alert.present();
  });
    }}
  */
}

